# javascript-example-program

This program is based off of the program available at [scoutlife.org](https://scoutlife.org/about-scouts/merit-badge-resources/programming/41261/javascript/). I didn't see a license so I assume it is in the open domain.
This program was made to demonstrate my ability to modify and debug JavaScript code. I am a Boy Scout and I am using it for the Programming Merit Badge. If you use this modified code in any of your own projects, please credit me in your project description or a dedicated credits page.

Try it out: https://turphin.gitlab.io/javascript-example-program

For a detailed explanation of this program, please see my article on Confluence: https://turtledev.atlassian.net/l/cp/aL70qDdt
